package org.example.repository;

import org.example.entity.Branch;
import org.hibernate.SessionFactory;

public class BranchRepository  extends GenericAbstractRepository <Branch>{

    public BranchRepository (SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    // select t from Branch t
    public String getEntityName() {
        return "Branch";
    }

    public Class<Branch> getEntityClass() {
        return Branch.class;
    }


}

