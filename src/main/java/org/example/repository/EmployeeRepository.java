package org.example.repository;

import org.example.entity.Employee;
import org.hibernate.SessionFactory;

public class EmployeeRepository extends GenericAbstractRepository <Employee>{
    public  ProductRepository (SessionFactory sessionFactory){
        super(sessionFactory);
    }

    @Override
    public String getEntityName() {
        return "Employee";
    }

    @Override
    public Class<Employee> getEntityClass() {
        return Employee.class;
    }

}

