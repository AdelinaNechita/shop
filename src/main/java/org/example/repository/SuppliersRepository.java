package org.example.repository;

import org.example.entity.Suppliers;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class SuppliersRepository {

    private SessionFactory sessionFactory;

    public SuppliersRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    public Suppliers createSuppliers(Suppliers suppliers) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(suppliers);
        transaction.commit();
        session.close();
        return suppliers;
    }

    public Suppliers readById(Integer id) {
        Session session = sessionFactory.openSession();
        Suppliers suppliers = session.find(Suppliers.class, id);
        session.close();
        return suppliers;

    }


    public List<Suppliers> readAll() {
        Session session = sessionFactory.openSession();
        List<Suppliers> suppliersList = session.createQuery("select s from Suppliers s", Suppliers.class).getResultList();
        session.close();
        return (suppliersList);
    }

    public Suppliers updateSuppliersDetails(Suppliers s) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        s = session.merge(s);
        transaction.commit();
        session.close();
        return s;
    }

    public void deleteSuppliers(Suppliers suppliers) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(suppliers);
        transaction.commit();
        session.close();
    }
}
