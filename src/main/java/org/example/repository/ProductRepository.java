package org.example.repository;

import org.example.entity.Product;
import org.hibernate.SessionFactory;

public class ProductRepository extends GenericAbstractRepository <Product>{
public ProductRepository (SessionFactory sessionFactory){
    super(sessionFactory);
}

    @Override
    public String getEntityName() {
        return "Product";
    }

    @Override
    public Class<Product> getEntityClass() {
        return Product.class;
    }
}
