package org.example.repository;

import org.example.entity.Client;
import org.hibernate.SessionFactory;

public class ClientRepository extends GenericAbstractRepository <Client>{
   public ClientRepository(SessionFactory sessionFactory){
       super(sessionFactory);
   }

    @Override
    public String getEntityName() {
        return "Client";
    }

    @Override
    public Class<Client> getEntityClass() {
        return Client.class;
    }
    }

