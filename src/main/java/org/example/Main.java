package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entity.Branch;
import org.example.entity.Suppliers;
import org.example.repository.BranchRepository;
import org.example.repository.SuppliersRepository;
import org.hibernate.SessionFactory;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        System.out.println("Try to open session Factory");
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        System.out.println("Session created");

        BranchRepository branchRepository = new BranchRepository(sessionFactory);
        Branch branch1 = new Branch(null, "Poema", "Brasov");
        branchRepository.create(branch1);

        Branch branch2 = new Branch(null, "dm", "craiova");
        branchRepository.create(branch2);

        Branch branch3 = new Branch(null, "Adidas", " Mall");
        branchRepository.create(branch3);

        Branch readBranch = branchRepository.readBy(2);
        System.out.println(readBranch);

        List<Branch> readAllBranches = branchRepository.readAll();
        System.out.println(readAllBranches);

        branch2.setName("Auchan Self Service");
        branchRepository.updateDetails(branch2);

        branchRepository.delete(branch1);
        System.out.println(branchRepository.readAll());


        SuppliersRepository suppliersRepository = new SuppliersRepository(sessionFactory);
        Suppliers suppliers1 = new Suppliers(null, "123",  "Brasov");
        suppliersRepository.createSuppliers(suppliers1);

        Suppliers suppliers2 = new Suppliers(null, "adjji", "Sinaia");
        suppliersRepository.createSuppliers(suppliers2);

        Suppliers suppliers3 = new Suppliers(null, "asda",  "Bran");
        suppliersRepository.createSuppliers(suppliers3);

        Suppliers readSuppliers = suppliersRepository.readById(3);
        System.out.println(readSuppliers);

        List<Suppliers> readAllSuppliers = suppliersRepository.readAll();
        System.out.println(readAllSuppliers);

        suppliers2.setName("Real Retail");
        suppliersRepository.updateSuppliersDetails(suppliers2);

        suppliersRepository.deleteSuppliers(suppliers1);
        System.out.println(suppliersRepository.readAll());

    }


}