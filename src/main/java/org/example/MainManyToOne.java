package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entity.Branch;
import org.example.entity.Employee;
import org.example.entity.Enums.Job;
import org.example.entity.Product;
import org.example.repository.BranchRepository;
import org.example.repository.EmployeeRepository;
import org.example.repository.ProductRepository;
import org.example.repository.SuppliersRepository;

import java.util.List;


public class MainManyToOne {
    public static void main(String[] args) {

        BranchRepository branchRepository = new branchRepository(HibernateConfiguration.getSessionFactory());
        EmployeeRepository employeeRepository = new employeeRepository(HibernateConfiguration.getSessionFactory());


        Branch branch1 = new Branch( null, "Selgros", "Bucuresti");
        Branch branch2 = new Branch( null, "Metro", "Brasov");

        branchRepository.create(branch1);
        branchRepository.create(branch2);

        Employee employee1= new Employee(null,"Buda Cristian", Job.CASIER, branch1 );
        Employee employee2= new Employee(null,"Marius Dumitrache",Job.MANAGER, branch2);
        Employee employee3= new Employee(null,"Marian Frizeru",Job.OTHER, branch2);
        Employee employee4= new Employee(null,"Ombladon Cheloo",Job.CASIER, branch2);

        employeeRepository.create(employee1);
        employeeRepository.create(employee2);
        employeeRepository.create(employee3);
        employeeRepository.create(employee4);


        List<Employee> readAllEmployeeList = employeeRepository.readAll();
        System.out.println(readAllEmployeeList);

        ProductRepository productRepository = new ProductRepository(HibernateConfiguration.getSessionFactory());
        SuppliersRepository suppliersRepository = new SuppliersRepository(HibernateConfiguration.getSessionFactory());

        Product product1 = new Product(null, "Bisc Croco", "Croco", 10.00, 5.0, 3);
        Product product2 = new Product(null, "Bisc Croco", "Croco", 10.00, 5.0, 3 );
    }
}
