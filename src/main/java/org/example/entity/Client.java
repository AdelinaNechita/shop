package org.example.entity;

import jakarta.persistence.*;

@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false)
    private String name;
    private String email;
    private Integer dataNasterii;

    public Client(Integer id, String name, String email, Integer dataNasterii) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.dataNasterii = dataNasterii;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getDataNasterii() {
        return dataNasterii;
    }

    public void setDataNasterii(Integer dataNasterii) {
        this.dataNasterii = dataNasterii;
    }
}
