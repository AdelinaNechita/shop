package org.example.entity;

import jakarta.persistence.*;

@Entity
public class Servicii {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    @Column(nullable = false)
    private String clothes;
    private String delivery;
    private String cleanning;

    public Servicii(String clothes, String delivery, String cleanning) {
        this.clothes = clothes;
        this.delivery = delivery;
        this.cleanning = cleanning;
    }

    public String getClothes() {
        return clothes;
    }

    public void setClothes(String clothes) {
        this.clothes = clothes;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getCleanning() {
        return cleanning;
    }

    public void setCleanning(String cleanning) {
        this.cleanning = cleanning;
    }
}
