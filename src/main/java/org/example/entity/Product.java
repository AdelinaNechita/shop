package org.example.entity;

import jakarta.persistence.*;

@Entity

public class Product {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)

    private Integer id;
@Column(nullable = false)
    private String name;
    private String brand;
    private Double price;
    @Column(name = "value_price")
    private Double sellingPrice;
    private Integer quantity;


    public Product(Integer id, String name, String brand, Double price, Double sellingPrice, Integer quantity) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.sellingPrice = sellingPrice;
        this.quantity = quantity;
    }

    public Product() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
