package generics;

public abstract class GenericDataHandler<T> {
    public double calculPretVanzare(double pretAchizitie, double cantitate, double adaosComercial) {


        return pretAchizitie * cantitate * (1 + adaosComercial);
    }
    public abstract String comp(T o1, T o2);
}
