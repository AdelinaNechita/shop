package generics;

public class ZaharDataHandle extends GenericDataHandler<Zahar> {
    public String comp(Zahar f1, Zahar f2) {
        if (f1.pret < f2.pret) {
            return "a doua este mai buna";
        } else {
            return "prima e mai buna";
        }
    }
}

